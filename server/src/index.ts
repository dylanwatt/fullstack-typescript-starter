import * as Koa from 'koa';
import * as Router from 'koa-router';
import * as stream from 'stream';

const app = new Koa();
const router = new Router();

router.get('/*', async (ctx) => {
    ctx.body = 'hello'
});

app.use(router.routes());

app.listen(3000);

console.log('Server running on port 3000');
