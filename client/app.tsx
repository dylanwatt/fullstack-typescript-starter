declare var require: any
require('./styles/base.scss');

import * as React from "react";
import * as ReactDOM from "react-dom";
import { Hello } from "./components/Hello";
import * as c from "common";

console.log(c);

ReactDOM.render(
    <div className="container">
      <Hello compiler="TypeScript" framework="React" />
    </div>,
    document.getElementById("example")
);
