import * as React from "react";
declare var CONFIG: any;

export interface HelloProps { compiler: string; framework: string; }

export const Hello = (props: HelloProps) => {
  return <div>
    <h1>Hello { CONFIG.env } there from {props.compiler} and {props.framework}!</h1>
  </div>
};
